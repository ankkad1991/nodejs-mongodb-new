FROM node:10
WORKDIR /test
COPY package.json /test
RUN npm install
COPY . /test
CMD node node.js
EXPOSE 9001
EXPOSE 9002